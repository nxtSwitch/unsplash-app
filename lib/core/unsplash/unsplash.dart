import 'dart:io';
import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as _http;

import 'exceptions/api_exception.dart';
import 'exceptions/offline_exception.dart';
import 'exceptions/response_format_exception.dart';
import 'exceptions/server_unavailable_exception.dart';

class Unsplash {
  final String baseUrl;
  final String accessKey;

  Unsplash(this.baseUrl, this.accessKey);

  Future<dynamic> fetch(String path, [Map<String, String> params]) async {
    final authHeader = {'Authorization': 'Client-ID $accessKey'};
    final requestUrl = Uri.https(baseUrl, path, params).toString();

    _http.Response response;
    try {
      response = await _http.get(requestUrl, headers: authHeader);
    } on SocketException catch (_) {
      throw OfflineException(requestUrl);
    } on HttpException catch (_) {
      throw ServerUnavailableException(requestUrl);
    } on TimeoutException catch (_) {
      throw ServerUnavailableException(requestUrl);
    }

    return _transformResponse(response);
  }

  dynamic _transformResponse(_http.Response response) {
    var data;
    try {
      data = json.decode(response.body);
    } catch (_) {
      throw ResponseFormatException(response.body);
    }

    if (data is Map && data.containsKey('errors')) {
      throw UnsplashApiException(
          response.statusCode, response.reasonPhrase, data['errors']);
    }

    return data;
  }
}
