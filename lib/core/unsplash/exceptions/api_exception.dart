class UnsplashApiException implements Exception {
  final int code;
  final String message;
  final List<String> errors;

  UnsplashApiException(this.code, this.message, this.errors);

  @override
  String toString() => message;
}
