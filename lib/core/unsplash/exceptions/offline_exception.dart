import 'dart:io';

class OfflineException extends HttpException {
  OfflineException(String uri)
      : super('Нет подключения к интернету.', uri: Uri.parse(uri));

  @override
  String toString() => message;
}
