import 'dart:io';

class ServerUnavailableException extends HttpException {
  ServerUnavailableException(String uri)
      : super('Сервер не отвечает.', uri: Uri.parse(uri));

  @override
  String toString() => message;
}
