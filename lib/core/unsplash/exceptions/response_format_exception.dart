class ResponseFormatException implements Exception {
  final String message;

  ResponseFormatException([this.message = 'Неизвестный формат ответа.']);

  @override
  String toString() => message;
}
