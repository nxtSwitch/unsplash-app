import 'dart:convert';
import 'package:flutter/services.dart';

class Config {
  static Future<Map<String, dynamic>> load(String path) async {
    try {
      final data = await rootBundle.loadString(path);
      final config = json.decode(data);

      return config;
    } catch (_) {
      return null;
    }
  }
}
