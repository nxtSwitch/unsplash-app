import 'dart:async';

class Debounce {
  Timer _debounce;

  final Function action;
  final int milliseconds;

  Debounce(this.action, {this.milliseconds = 300});

  void call() {
    if (_debounce != null) _debounce.cancel();
    _debounce = Timer(Duration(milliseconds: milliseconds), action);
  }
}
