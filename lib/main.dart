import 'package:flutter/material.dart';

import 'app/app.dart';

import 'view/view.dart';
import 'view/injector.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final app = App(configPath: 'config/unsplash.json');
  await app.start();

  runApp(AppInjector(child: AppView(), app: app));
}
