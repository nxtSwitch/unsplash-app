import '../../core/unsplash/unsplash.dart';
import '../models/unsplash_photo_model.dart';

// enum UnsplashPhotosOrderBy { latest, oldest, popular }

class UnsplashPhotosRemote {
  final Unsplash _unsplashClient;

  UnsplashPhotosRemote(this._unsplashClient);

  Future<List<UnsplashPhotoModel>> photos({
    int page = 1,
    int perPage = 10,
    String orderBy = 'latest',
  }) async {
    final photos = <UnsplashPhotoModel>[];

    final data = await _unsplashClient.fetch(
      'photos',
      {'page': '$page', 'per_page': '$perPage', 'order_by': '$orderBy'},
    );

    if (data is! List) {
      throw Exception('Неизвестный формат ответа.');
    }

    for (final item in data) {
      photos.add(UnsplashPhotoModel.fromJson(item));
    }

    return photos;
  }
}
