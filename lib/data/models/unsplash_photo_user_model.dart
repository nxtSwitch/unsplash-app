class UnsplashPhotoUserModel {
  final String id;
  final String name;
  final String username;

  UnsplashPhotoUserModel({this.id, this.name, this.username});

  factory UnsplashPhotoUserModel.fromJson(Map<String, dynamic> json) =>
      UnsplashPhotoUserModel(
        id: json['id'],
        name: json['name'],
        username: json['username'],
      );

  String toString() => 'User { username: $username }';
}
