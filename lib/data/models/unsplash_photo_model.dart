import 'unsplash_photo_urls_model.dart';
import 'unsplash_photo_user_model.dart';

class UnsplashPhotoModel {
  final String id;
  final String description;
  final UnsplashPhotoUserModel user;
  final UnsplashPhotoUrlsModel urls;

  UnsplashPhotoModel({this.id, this.description, this.user, this.urls});

  factory UnsplashPhotoModel.fromJson(Map<String, dynamic> json) =>
      UnsplashPhotoModel(
        id: json['id'],
        description: json['description'] ?? '',
        user: UnsplashPhotoUserModel.fromJson(json['user']),
        urls: UnsplashPhotoUrlsModel.fromJson(json['urls']),
      );

  String toString() =>
      'Photo { description: $description, user: $user, urls: $urls }';
}
