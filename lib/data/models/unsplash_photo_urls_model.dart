class UnsplashPhotoUrlsModel {
  final String raw;
  final String full;
  final String regular;
  final String small;
  final String thumb;

  UnsplashPhotoUrlsModel(
      {this.raw, this.full, this.regular, this.small, this.thumb});

  factory UnsplashPhotoUrlsModel.fromJson(Map<String, dynamic> json) =>
      UnsplashPhotoUrlsModel(
        raw: json['raw'],
        full: json['full'],
        regular: json['regular'],
        small: json['small'],
        thumb: json['thumb'],
      );

  String toString() => 'Urls { raw: $raw }';
}
