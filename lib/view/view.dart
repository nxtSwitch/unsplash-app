import 'package:flutter/material.dart';

import 'routes.dart';
import 'injector.dart';
import 'styles/themes.dart';

import '../app/constants/strings.dart';

class AppView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: AppThemes.main,
      title: AppStrings.appTitle,
      //
      routes: Routes.getRoutes(),
      initialRoute: Routes.photosList,
      //
      debugShowCheckedModeBanner: !AppInjector.of(context).app.isProduction,
    );
  }
}
