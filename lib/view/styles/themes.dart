import 'package:flutter/material.dart';

class AppThemes {
  static const mainColor = const Color.fromRGBO(17, 17, 17, 1);

  static final main = ThemeData(
    canvasColor: mainColor,
    primaryColor: mainColor,
    accentColor: Colors.white,
    splashColor: Colors.transparent,
    textTheme: TextTheme(
      bodyText1: TextStyle(
        color: Colors.white,
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
      subtitle1: TextStyle(
        color: Colors.white,
        fontSize: 14,
        fontWeight: FontWeight.normal,
      ),
    ),
  );
}
