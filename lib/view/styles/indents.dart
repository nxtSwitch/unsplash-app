class AppIndents {
  static const lower = 4.0;
  static const xsmall = 8.0;
  static const small = 12.0;
  static const medium = 16.0;
  static const large = 24.0;
  static const xlarge = 32.0;
}
