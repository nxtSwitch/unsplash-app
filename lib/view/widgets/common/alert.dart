import 'package:flutter/material.dart';

class Alert extends SnackBar {
  Alert(String text)
      : super(
            content: Text(text),
            duration: Duration(days: 1),
            backgroundColor: Colors.black.withOpacity(.6));
}
