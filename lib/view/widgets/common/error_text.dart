import 'package:flutter/material.dart';

class ErrorText extends StatelessWidget {
  final String text;

  ErrorText(this.text);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        text,
        maxLines: 1,
        overflow: TextOverflow.fade,
        style: Theme.of(context).textTheme.bodyText1,
      ),
    );
  }
}
