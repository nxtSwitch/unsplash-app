import 'package:flutter/material.dart';

import '../photo/photo_card.dart';
import '../../styles/indents.dart';
import '../../../core/helpers/debounce.dart';
import '../../../data/models/unsplash_photo_model.dart';

class PhotosListLayout extends StatelessWidget {
  final Debounce _debounce;

  final List<UnsplashPhotoModel> photos;

  final Function() onListScrollEnd;
  final Function(UnsplashPhotoModel) onPhotoTap;

  PhotosListLayout(this.photos, {this.onPhotoTap, this.onListScrollEnd})
      : this._debounce = Debounce(onListScrollEnd);

  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size.width * .7;

    return NotificationListener(
      child: ListView.builder(
        itemCount: photos.length,
        padding: EdgeInsets.only(
          top: AppIndents.medium,
        ),
        itemBuilder: (_, index) =>
            PhotoCard(photos[index], size: size, onTap: onPhotoTap),
      ),
      onNotification: _onScrollNotification,
    );
  }

  bool _onScrollNotification(ScrollNotification scroll) {
    if (scroll.metrics.pixels == scroll.metrics.maxScrollExtent) {
      _debounce.call();
    }
    return true;
  }
}
