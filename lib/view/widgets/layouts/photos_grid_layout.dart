import 'package:flutter/material.dart';

import '../photo/photo_card.dart';
import '../../styles/indents.dart';
import '../../../core/helpers/debounce.dart';
import '../../../data/models/unsplash_photo_model.dart';

class PhotosGridLayout extends StatelessWidget {
  final Debounce _debounce;

  final List<UnsplashPhotoModel> photos;

  final int columns;
  final Function() onGridScrollEnd;
  final Function(UnsplashPhotoModel) onPhotoTap;

  PhotosGridLayout(this.photos,
      {this.columns = 2, this.onPhotoTap, this.onGridScrollEnd})
      : this._debounce = Debounce(onGridScrollEnd);

  Widget build(BuildContext context) {
    final paddings = AppIndents.medium * (columns * 3);
    final size = (MediaQuery.of(context).size.width - paddings) / columns;

    return NotificationListener(
      child: GridView.builder(
        itemCount: photos.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: columns,
        ),
        padding: EdgeInsets.only(
          top: AppIndents.medium,
        ),
        itemBuilder: (_, index) =>
            PhotoCard(photos[index], size: size, onTap: onPhotoTap),
      ),
      onNotification: _onScrollNotification,
    );
  }

  bool _onScrollNotification(ScrollNotification scroll) {
    if (scroll.metrics.pixels == scroll.metrics.maxScrollExtent) {
      _debounce.call();
    }
    return true;
  }
}
