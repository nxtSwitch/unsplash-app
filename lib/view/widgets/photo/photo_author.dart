import 'package:flutter/material.dart';

import '../../styles/indents.dart';
import '../../../data/models/unsplash_photo_user_model.dart';

class PhotoAuthor extends StatelessWidget {
  final UnsplashPhotoUserModel user;

  PhotoAuthor(this.user);

  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: AppIndents.large,
      ),
      child: Text(
        user.name,
        maxLines: 1,
        overflow: TextOverflow.fade,
        style: Theme.of(context).textTheme.subtitle1,
      ),
    );
  }
}
