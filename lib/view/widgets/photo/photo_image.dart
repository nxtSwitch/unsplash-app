import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

class PhotoImage extends StatelessWidget {
  final String tag;
  final String image;

  PhotoImage(this.image, this.tag);

  Widget build(BuildContext context) {
    return Hero(
      tag: tag,
      child: FadeInImage.memoryNetwork(
        image: image,
        fit: BoxFit.cover,
        placeholder: kTransparentImage,
      ),
    );
  }
}
