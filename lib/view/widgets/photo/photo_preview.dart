import 'package:flutter/material.dart';

import 'photo_image.dart';
import '../../styles/indents.dart';
import '../../../data/models/unsplash_photo_model.dart';

class PhotoPreview extends StatelessWidget {
  final double size;
  final UnsplashPhotoModel photo;
  final Function(UnsplashPhotoModel) onTap;

  PhotoPreview(this.photo, {this.onTap, this.size});

  @override
  Widget build(BuildContext context) {
    return Padding(
      child: InkWell(
        borderRadius: BorderRadius.circular(AppIndents.small),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(AppIndents.small),
          child: Stack(
            alignment: Alignment.bottomLeft,
            children: <Widget>[
              Positioned.fill(child: PhotoImage(photo.urls.small, photo.id)),
              _textBackground(size),
              _text(photo.description, Theme.of(context).textTheme.bodyText1),
            ],
          ),
        ),
        onTap: () => onTap(photo),
      ),
      padding: EdgeInsets.symmetric(
        horizontal: AppIndents.medium,
        vertical: AppIndents.small,
      ),
    );
  }

  Widget _text(String text, TextStyle style) {
    return Padding(
      padding: EdgeInsets.all(AppIndents.medium),
      child: Text(text, maxLines: 1, overflow: TextOverflow.fade, style: style),
    );
  }

  Widget _textBackground(double size) {
    return Container(
      height: size,
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: FractionalOffset.topCenter,
            end: FractionalOffset.bottomCenter,
            colors: [Colors.transparent, Colors.black.withOpacity(.7)],
            stops: [0.0, 1.0]),
      ),
    );
  }
}
