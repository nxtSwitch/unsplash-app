import 'package:flutter/material.dart';

import 'photo_author.dart';
import 'photo_preview.dart';

import '../../../data/models/unsplash_photo_model.dart';

class PhotoCard extends StatelessWidget {
  final double size;
  final UnsplashPhotoModel photo;
  final Function(UnsplashPhotoModel) onTap;

  PhotoCard(this.photo, {this.size, this.onTap});

  Widget build(BuildContext context) {
    return Column(
      children: [
        PhotoAuthor(photo.user),
        PhotoPreview(photo, onTap: onTap, size: size),
      ],
      crossAxisAlignment: CrossAxisAlignment.start,
    );
  }
}
