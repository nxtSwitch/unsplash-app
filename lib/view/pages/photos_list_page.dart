import 'dart:async';
import 'package:flutter/material.dart';

import 'photo_page.dart';

import '../widgets/common/alert.dart';
import '../widgets/common/loading_indicator.dart';
import '../widgets/layouts/photos_list_layout.dart';
import '../widgets/layouts/photos_grid_layout.dart';

import '../injector.dart';
import '../../app/constants/strings.dart';
import '../../data/models/unsplash_photo_model.dart';
import '../../app/blocs/photos_bloc.dart';

class PhotosListPage extends StatefulWidget {
  static const routeName = '/photoList';

  PhotosListPage({Key key}) : super(key: key);

  @override
  _PhotosListPage createState() => _PhotosListPage();
}

class _PhotosListPage extends State<PhotosListPage> {
  PhotosBloc _photosBloc;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();

    final app = AppInjector.of(context).app;
    _photosBloc = PhotosBloc(app.unsplashPhotosRemote);

    _photosBloc.stream
        .where((state) => state is PhotosLoadFailureState)
        .cast<PhotosLoadFailureState>()
        .listen((state) => _showSnackBar(state.errorText));

    _photosBloc.stream
        .where((state) => state is PhotosLoadInProgressState)
        .cast<PhotosLoadInProgressState>()
        .listen((state) => _showSnackBar(AppStrings.loadingAlert));

    _photosBloc.loadNextPage();
  }

  @override
  Widget build(BuildContext context) {
    final photosStream =
        _photosBloc.stream.where((state) => state is PhotosLoadSuccessState);

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(AppStrings.appTitle),
      ),
      body: StreamBuilder(
        stream: photosStream,
        initialData: PhotosInitialState(),
        builder: (context, snapshot) {
          if (snapshot.data is PhotosInitialState) {
            return LoadingIndicator();
          }

          Future.delayed(Duration(milliseconds: 200), _hideSnackBar);
          return _photosLayout(snapshot.data.photos);
        },
      ),
    );
  }

  Widget _photosLayout(List<UnsplashPhotoModel> photos) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth <= 460) {
          return PhotosListLayout(
            photos,
            onPhotoTap: _onPhotoCardTap,
            onListScrollEnd: _photosBloc.loadNextPage,
          );
        }

        return PhotosGridLayout(
          photos,
          columns: 3,
          onPhotoTap: _onPhotoCardTap,
          onGridScrollEnd: _photosBloc.loadNextPage,
        );
      },
    );
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState.hideCurrentSnackBar();
    _scaffoldKey.currentState.showSnackBar(Alert(text));
  }

  void _hideSnackBar() async {
    _scaffoldKey.currentState.hideCurrentSnackBar();
  }

  void _onPhotoCardTap(UnsplashPhotoModel photo) {
    Navigator.of(context).pushNamed(PhotoPage.routeName, arguments: photo);
  }

  @override
  void dispose() {
    _photosBloc.dispose();
    super.dispose();
  }
}
