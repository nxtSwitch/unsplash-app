import 'package:flutter/material.dart';

import '../widgets/common/loading_indicator.dart';
import '../widgets/photo/photo_image.dart';

import '../../data/models/unsplash_photo_model.dart';

class PhotoPage extends StatefulWidget {
  static const routeName = '/photo';

  PhotoPage({Key key}) : super(key: key);

  @override
  _PhotoPage createState() => _PhotoPage();
}

class _PhotoPage extends State<PhotoPage> {
  @override
  Widget build(BuildContext context) {
    final UnsplashPhotoModel photo = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Center(child: LoadingIndicator()),
          InteractiveViewer(
            child: Center(child: PhotoImage(photo.urls.regular, photo.id)),
          ),
        ],
      ),
    );
  }
}
