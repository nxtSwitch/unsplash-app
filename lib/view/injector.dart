import 'package:flutter/widgets.dart';

import '../app/app.dart';

class AppInjector extends InheritedWidget {
  final App app;

  AppInjector({Widget child, this.app}) : super(child: child);

  @override
  bool updateShouldNotify(AppInjector old) => false;

  static AppInjector of(BuildContext context) =>
      context.getElementForInheritedWidgetOfExactType<AppInjector>()?.widget;
}
