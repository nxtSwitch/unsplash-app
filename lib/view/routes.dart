import 'package:flutter/widgets.dart';

import 'pages/photo_page.dart';
import 'pages/photos_list_page.dart';

class Routes {
  static const String photo = PhotoPage.routeName;
  static const String photosList = PhotosListPage.routeName;

  static Map<String, WidgetBuilder> getRoutes() {
    return {
      Routes.photo: (context) => PhotoPage(),
      Routes.photosList: (context) => PhotosListPage(),
    };
  }
}
