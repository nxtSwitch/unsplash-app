import 'package:flutter/foundation.dart';

import '../core/helpers/config.dart';
import '../core/unsplash/unsplash.dart';

import '../data/remotes/unsplash_photos_remote.dart';

class App {
  Unsplash _unsplash;
  UnsplashPhotosRemote _unsplashPhotosRemote;

  final String configPath;
  final bool isProduction = kReleaseMode;

  UnsplashPhotosRemote get unsplashPhotosRemote => _unsplashPhotosRemote;

  App({this.configPath});

  Future start() async {
    final config = await Config.load(configPath);

    if (config == null) {
      throw Exception('Не найден файл конфигурации.');
    }

    _unsplash = Unsplash(config['baseUrl'], config['accessKey']);
    _unsplashPhotosRemote = UnsplashPhotosRemote(_unsplash);
  }
}
