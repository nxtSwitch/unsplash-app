import 'dart:io';
import 'dart:async';

import '../../data/models/unsplash_photo_model.dart';
import '../../data/remotes/unsplash_photos_remote.dart';

class PhotosBloc {
  int _page = 0;

  bool _hasError = false;
  bool _isLoading = false;
  bool _hasReachedMax = false;

  final _photos = List<UnsplashPhotoModel>();
  final UnsplashPhotosRemote _unsplashPhotosRemote;
  final _streamController = StreamController<PhotosState>.broadcast();

  Stream<PhotosState> get stream => _streamController.stream;

  PhotosBloc(this._unsplashPhotosRemote);

  void loadNextPage() async {
    if (_streamController.isClosed) return;
    if (_isLoading || _hasReachedMax) return;

    _isLoading = true;
    if (!_hasError && _page > 0) {
      _streamController.add(PhotosLoadInProgressState());
    }

    try {
      final photos = await _unsplashPhotosRemote.photos(page: _page);

      _page++;
      _hasError = false;
      _photos.addAll(photos);

      _streamController.add(PhotosLoadSuccessState(_photos));
    } on HttpException catch (error) {
      _hasError = true;
      _streamController.add(PhotosLoadFailureState('$error'));
    }
    // API перестает отдавать лист, если больше нет страниц
    catch (_) {
      _hasReachedMax = true;
    } finally {
      _isLoading = false;
    }
  }

  Future<void> dispose() async {
    _photos.clear();
    await _streamController.close();
  }
}

abstract class PhotosState {}

class PhotosInitialState extends PhotosState {}

class PhotosLoadSuccessState extends PhotosState {
  final List<UnsplashPhotoModel> photos;
  PhotosLoadSuccessState(this.photos);
}

class PhotosLoadFailureState extends PhotosState {
  final String errorText;
  PhotosLoadFailureState(this.errorText);
}

class PhotosLoadInProgressState extends PhotosState {}
